class Algorithm < ApplicationRecord
  has_and_belongs_to_many :roles, join_table: 'roles_algorithms'

serialize :arguments, JSON
end
