class Role < ApplicationRecord
  has_and_belongs_to_many :users, join_table: 'roles_users'
  has_and_belongs_to_many :algorithms, join_table: 'roles_algorithms'
end
