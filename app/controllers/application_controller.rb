class ApplicationController < ActionController::API
  def create_message
    render json: Message.create(params.permit('author', 'message'))
  end

  def get_messages
    co = ChatOnline.find_or_create_by(author: params[:author], uuid: params[:uuid])
    co.touch
    render json: {
      messages: Message.all.order('id DESC').limit(30).reverse,
      online: ChatOnline.where('updated_at > ?', Time.now.to_i - 10).where.not(author: 'noname').pluck(:author)
    }
  end
end
