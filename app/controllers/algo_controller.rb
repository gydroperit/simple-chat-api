class AlgoController < ApplicationController
  before_action :set_user, except: %i[login get_users create_algo create_user]

  # algo  + roles_ids
  def create_algo; end

  def create_user
    user = User.find_or_create_by(id: params[:id])
    user.update(params.require(:algo).permit(:name, :password, :family, :email))
    user.roles = Role.where(id: params.require(:algo).permit![:role_ids])
    user.save
    render json: User.all.includes(:roles), include: [:roles]
  end

  def get_users
    render json: User.all.includes(:roles), include: [:roles]
  end

  def login
    render json: User.find_by(password: params[:password], email: params[:email]), include: [:roles]
  end

  def algos
    render json: @user.algorithms.distinct
  end

  def create_algo
    algo  = Algorithm.create(params.require(:algo).permit!)
    algo.roles = Role.where(id: params.require(:algo).permit![:role_ids])
    algo.save
  end
  def set_user
    @user = User.find(params[:user_id])
  end
end
