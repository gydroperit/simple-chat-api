class Addref < ActiveRecord::Migration[5.1]
  def change
    create_table :roles_users do |t|
      t.integer :user_id
      t.integer :role_id
    end
    create_table :roles_algorithms do |t|
      t.integer :algorithm_id
      t.integer :role_id
    end
  end
end
