class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :author, default: 'noname', null: false
      t.string :message, default: '', null: false
      t.integer :created_at, null: false
    end
  end
end
