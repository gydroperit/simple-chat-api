class CreateChatOnlines < ActiveRecord::Migration[5.1]
  def change
    create_table :chat_onlines do |t|
      t.string :author, default: 'noname', null: false
      t.integer :updated_at, null: false
    end
  end
end
