class CreateAlgorithms < ActiveRecord::Migration[5.1]
  def change
    create_table :algorithms do |t|
      t.text :code
      t.string :name
      t.text :arguments
      t.timestamps
    end
  end
end
