require 'capun/setup'

# name of the user at server who would own the project
set :user, 'qsion'
# ip for the server where the project would be deployed
server '212.32.240.218', user: fetch(:user), roles: %w{web app db}

# path to the git repository
set :repo_url, 'git@gitlab.com:gydroperit/simple-chat-api.git'
set :branch, 'master'

set :rvm1_ruby_version, "ruby-2.4.0"

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets public/system}
# after 'start', 'your_task'  # you can set additional capistrano tasks to start server properly
