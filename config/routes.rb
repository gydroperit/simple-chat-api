Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'message' => 'application#create_message'
  get 'messages/:author' => 'application#get_messages'
  get 'users' => 'algo#get_users'
  get 'algos' => 'algo#algos'
  post 'algo' => 'algo#create_algo'

  post 'user_create' => 'algo#create_user'
  post 'login' => 'algo#login'
end
